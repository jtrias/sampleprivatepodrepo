Pod::Spec.new do |s|
  s.name             = "SamplePodLib"
  s.version          = "0.1.2"
  s.summary          = "The SamplePodLib is a sample library created to test the private Pods."
  s.description      = <<-DESC
                       ### SamplePodLib ###
                       We created the SamplePodLib to test a private Pods repo
                       DESC
  s.homepage         = "https://bitbucket.org/jtrias/samplepodlib"
  # s.screenshots      = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Juan Trías" => "juan.trias@intelygenz.com" }
  s.source           = { :git => "https://jtrias@bitbucket.org/jtrias/samplepodlib.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/NAME'

  # s.platform     = :ios, '5.0'
  # s.ios.deployment_target = '5.0'
  # s.osx.deployment_target = '10.7'
  s.requires_arc = true

  s.source_files = 'Classes/**/*.{h,m}'
  # s.resources = 'Resources'

  s.ios.exclude_files = 'Classes/osx'
  s.osx.exclude_files = 'Classes/ios'
  # s.public_header_files = 'Classes/**/*.h'
  # s.frameworks = 'SomeFramework', 'AnotherFramework'
  # s.dependency 'JSONKit', '~> 1.4'
end
