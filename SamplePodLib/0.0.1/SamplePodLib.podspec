Pod::Spec.new do |s|
  s.name             = "SamplePodLib"
  s.version          = "0.0.1"
  s.summary          = "A short description of SamplePodLib."
  s.description      = <<-DESC
                       An optional longer description of SamplePodLib

                       * Markdown format.
                       * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "http://EXAMPLE/NAME"
  s.screenshots      = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Juan Trías" => "juan.trias@intelygenz.com" }
  s.source           = { :git => "https://jtrias@bitbucket.org/jtrias/samplepodlib.git", :commit => "e0c8b7575996ab2cb18e4400eac929971c99670a" }
  # s.social_media_url = 'https://twitter.com/NAME'

  # s.platform     = :ios, '5.0'
  # s.ios.deployment_target = '5.0'
  # s.osx.deployment_target = '10.7'
  s.requires_arc = true

  s.source_files = 'Classes/**/*.{h,m}'
  s.resources = 'Resources'

  s.ios.exclude_files = 'Classes/osx'
  s.osx.exclude_files = 'Classes/ios'
  # s.public_header_files = 'Classes/**/*.h'
  # s.frameworks = 'SomeFramework', 'AnotherFramework'
  # s.dependency 'JSONKit', '~> 1.4'
end
